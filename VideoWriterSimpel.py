import os
import cv2


class VideoWriter:
    """
    writer = VideoWriter(path=resultpath)
    for frame in some_code_that_generate_frames():
        writer.write('raw', frame)
        fancyframe = some_code_that_do_some_fancy_with_the_frame()
        writer.write('fancy', fancyframe)
    """
    def __init__(self, codec:str='XVID', codec_losless:str='png ', fps:int=30.0, path:str='') -> None:
        import atexit
        self.codec = codec
        self.codec_losless = codec_losless
        self.fps = fps
        self.path = path

        self.out_dict = {}

        atexit.register(self.close_all)

    def write(self, id:str, img, losless=False):
        try:
            out = self.out_dict[id]
        except:
            isColor = img.ndim == 3
            path_comb = os.path.join(self.path, id + '.avi')
            if losless:
                fourcc = cv2.VideoWriter_fourcc(*self.codec_losless)
            else:
                fourcc = cv2.VideoWriter_fourcc(*self.codec)
            out = cv2.VideoWriter(path_comb, fourcc, self.fps, (img.shape[1],img.shape[0]), isColor)
            self.out_dict.update({id: out})

        out.write(img)

    def close(self, id:str):
        out = self.out_dict.pop(id)
        out.release()


    def close_all(self):
        for out in self.out_dict.values():
            out.release()
